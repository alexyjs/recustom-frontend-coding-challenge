import type { Meta, StoryObj } from '@storybook/react';
import ActionableToast from './ActionableToast';
import { TOAST_TYPE } from '../NotiicationToast/NotificationToast';

const meta = {
  title: 'Toast',
  component: ActionableToast,
  tags: ['autodocs'],
  parameters: {
    layout: 'fullscreen',
  },
} satisfies Meta<typeof ActionableToast>;

export default meta;
type Story = StoryObj<typeof meta>;

export const SuccessActionableToastStory: Story = {
  args: {
    type: TOAST_TYPE.SUCCESSFUL,
    message: "Well done, you successfully read this important alert message. This example text is going to run a bit longer so that you can see how spacing within an alert works with this kind of content. Be sure to use margin utilities to keep things nice and tidy."
  },
};


export const ErrorActionableToastStory: Story = {
  args: {
    type: TOAST_TYPE.ERROR,
    message:"Oh snap, you successfully read this important alert message. This example text is going to run a bit longer so that you can see how spacing within an alert works with this kind of content. Be sure to use margin utilities to keep things nice and tidy."
  },
};
