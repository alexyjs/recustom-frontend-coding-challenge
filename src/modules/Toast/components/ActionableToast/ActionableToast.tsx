import * as React from "react";
import CheckIcon from "@mui/icons-material/Check";
import CloseIcon from "@mui/icons-material/Close";
import Image from "next/image";
import WarningIcon from "../../../../../public/warning.svg";
import {
  Container,
  Content,
  Footer,
  Header,
  IConContainer,
  TakeActionButton,
  Title,
} from "./themedStyled";
import { CloseButton } from "../NotiicationToast/themedStyled";
import { TOAST_TYPE } from "../NotiicationToast/NotificationToast";

export type ActionableToastProps = {
  type: TOAST_TYPE;
  message: string;
  onClose: () => void;
  onClick: () => void;
};

const ActionableToast = ({
  type,
  message,
  onClose,
  onClick,
}: ActionableToastProps) => {
  return (
    <Container type={type}>
      <Header>
        {type === TOAST_TYPE.SUCCESSFUL ? (
          <IConContainer>
            <CheckIcon />
          </IConContainer>
        ) : (
          <Image
            src={WarningIcon}
            alt="warning"
            width={18}
            height={18}
            priority
          />
        )}

        <Title>
          {type === TOAST_TYPE.SUCCESSFUL ? "Success" : "Attention"}
        </Title>
        <CloseButton toastType={type} onClick={onClose}>
          <CloseIcon color="inherit" />
        </CloseButton>
      </Header>
      <Content>{message}</Content>
      <Footer>
        <TakeActionButton toastType={type} onClick={onClick}>
          Take action
        </TakeActionButton>
      </Footer>
    </Container>
  );
};
export default ActionableToast;
