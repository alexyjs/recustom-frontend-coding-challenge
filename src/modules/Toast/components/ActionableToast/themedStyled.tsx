import styled from "@emotion/styled";
import { css } from "@emotion/react";
import { TOAST_TYPE } from "../NotiicationToast/NotificationToast";

const flex = css`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
`;

export const Container = styled.div<{ type: TOAST_TYPE }>`
  ${flex}
  width: 100%;
  padding: 1rem;
  box-sizing: border-box;
  font-family: TT Hoves Pro;
  color: ${({ type }) =>
    type === TOAST_TYPE.SUCCESSFUL ? "#00ac80" : "#FF6464"};
  flex-direction: column;
  gap: 6px;
`;

export const Header = styled.div`
  ${flex}
  gap: 8px;
`;
export const IConContainer = styled.div`
  ${flex}
  width: 18px;
  height: 18px;
  border-radius: 50%;
  background: #00ac80;
  svg {
    font-size: 12px;
    color: #ffff;
  }
`;

export const Title = styled.h2`
  margin: 0;
  width: calc(100% - 16px - 36px);
  font-size: 14px;
  line-height: 21px;

`;
export const Content = styled.p`
  margin: 0;
  width: 100%;
  font-size: 14px;
  font-weight: 400;
  line-height: 21px;
  letter-spacing: 0em;
  text-align: left;
  font-variation-settings: "wdth" 100, "CTGR" 0;
`;

export const Footer = styled.div`
  text-align: left;
  width: 100%;
  margin-top: 6px;
`;

export const TakeActionButton = styled.button<{ toastType: TOAST_TYPE }>`
  padding: 4px, 12px, 4px, 12px;
  box-sizing: border-box;
  border-radius: 100px;
  background: ${({ toastType }) =>
    toastType === TOAST_TYPE.SUCCESSFUL ? "#00ac80" : "#FF6464"};
  border: 1px solid #000000;
  color: #ffff;
  font-size: 12px;
  min-height: 26px;
  min-width: 91px;
  font-weight: 600;
`;
