import type { Meta, StoryObj } from '@storybook/react';
import MessageToast from './MessageToast';
import { TOAST_TYPE } from '../NotiicationToast/NotificationToast';

const meta = {
  title: 'Toast',
  component: MessageToast,
  tags: ['autodocs'],
  parameters: {
    layout: 'fullscreen',
  },
} satisfies Meta<typeof MessageToast>;

export default meta;
type Story = StoryObj<typeof meta>;

export const MessageToastStory: Story = {
  args: {
    avatar: "https://e7.pngegg.com/pngimages/799/987/png-clipart-computer-icons-avatar-icon-design-avatar-heroes-computer-wallpaper-thumbnail.png",
    name: "Bonnie Green",
    message: "Hi Neil, thanks for sharing your thoughts regardingFlowbite."
  },
};
