
import * as React from "react";
import CloseIcon from "@mui/icons-material/Close";
import Image from "next/image";
import { Container, ContentContainer, Footer, Message, Name, ActionButton, InfoContainer, CloseButton } from "./themedStyled";

export type MessageToastProps = {
  avatar: string;
  name: string;
  message: string;
  onClose: () => void;
  onClick: () => void;
};

const MessageToast = ({
  avatar,
  name,
  message,
  onClose,
  onClick
}: MessageToastProps) => {
  return (
    <Container>
      <ContentContainer>
        <Image src={avatar} alt="avatar" width={32} height={32} priority />
        <InfoContainer>
          <Name>{name}</Name>
          <Message>{message}</Message>
          <Footer>
            <ActionButton onClick={onClick}>Button text</ActionButton>
          </Footer>
        </InfoContainer>
      </ContentContainer>
      <CloseButton onClick={onClose}>
        <CloseIcon color="inherit" />
      </CloseButton>
    </Container>
  );
};
export default MessageToast;
