import styled from "@emotion/styled";
import { css } from "@emotion/react";

const flex = css`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
`;

export const Container = styled.div`
  ${flex}
  width: 100%;
  padding: 1rem;
  box-sizing: border-box;
  font-family: TT Hoves Pro;
  gap: 1rem;
  align-items: flex-start;
`;

export const ContentContainer = styled.div`
  ${flex}
  align-items: flex-start;
  gap: 12px;
  width: 100%;
`;

export const InfoContainer = styled.div`
  display: flex;
  flex-direction: column;
  gap: 4px;
  width: 100%;
`

export const Name = styled.p`
  margin: 0;
  font-size: 14px;
  line-height: 21px;
  color: #111928;
  font-weight: 600;
`;
export const Message = styled(Name)`
  font-weight: 400;
  font-variation-settings: "wdth" 100, "CTGR" 0;
  color: #6b7280;
`;

export const Footer = styled.div`
  text-align: left;
  width: 100%;
  margin-top: 8px;
`;

export const ActionButton = styled.button`
  padding: 4px, 12px, 4px, 12px;
  box-sizing: border-box;
  border-radius: 100px;
  background: #FFFF00;
  border: 1px solid #000000;
  color: #112127;
  font-size: 12px;
  line-height: 18px;
  min-height: 26px;
  min-width: 91px;
  font-weight: 600;
`;

export const CloseButton = styled.button`
  ${flex}
  width: 18px;
  height: 18px;
  border: none;
  background-color: transparent;
  color: #9da3af;
  cursor: pointer;
  svg {
    font-size: 18px;
  }
`;
