import type { Meta, StoryObj } from '@storybook/react';
import NotificationToast, { TOAST_TYPE } from './NotificationToast';

const meta = {
  title: 'Toast',
  component: NotificationToast,
  tags: ['autodocs'],
  parameters: {
    layout: 'fullscreen',
  },
} satisfies Meta<typeof NotificationToast>;

export default meta;
type Story = StoryObj<typeof meta>;

export const SuccessNotificationToastStory: Story = {
  args: {
    type: TOAST_TYPE.SUCCESSFUL,
    message:"The action that you have done was a success! Well done"
  },
};


export const ErrorNotificationToastStory: Story = {
  args: {
    type: TOAST_TYPE.ERROR,
    message: "The file flowbite-figma-pro was permanently deleted."
  },
};
