import * as React from "react";
import CheckIcon from "@mui/icons-material/Check";
import CloseIcon from "@mui/icons-material/Close";
import Image from "next/image";
import NotificationsIcon from "../../../../../public/notification.svg";
import {
  AlertContainer,
  CheckContainer,
  CloseButton,
  Message,
  RightBox,
} from "./themedStyled";

export enum TOAST_TYPE {
  SUCCESSFUL = "success",
  ERROR = "error",
}
export type NotificationToastProps = {
  type: TOAST_TYPE;
  message: string;
  onClose: () => void;
};

const NotificationToast = ({
  type,
  message,
  onClose,
}: NotificationToastProps) => {
  return (
    <AlertContainer type={type}>
      <CheckContainer type={type}>
        {type === TOAST_TYPE.SUCCESSFUL ? (
          <CheckIcon fontSize="small" color="inherit" />
        ) : (
          <Image
            src={NotificationsIcon}
            alt="notification"
            width={20}
            height={20}
            priority
          />
        )}
      </CheckContainer>
      <RightBox>
        <Message type={type}>{message}</Message>
        <CloseButton toastType={type} onClick={onClose}>
          <CloseIcon color="inherit" />
        </CloseButton>
      </RightBox>
    </AlertContainer>
  );
};
export default NotificationToast;
