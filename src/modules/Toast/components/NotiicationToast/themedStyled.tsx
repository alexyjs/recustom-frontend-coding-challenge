import styled from "@emotion/styled";
import { css } from "@emotion/react";
import { TOAST_TYPE } from "./NotificationToast";

const flex = css`
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const AlertContainer = styled.div<{ type: TOAST_TYPE }>`
  ${flex}
  border: 1px solid
    ${({ type }) => (type === TOAST_TYPE.SUCCESSFUL ? "#66e7c6" : "#FFA2A2")};
  background: #ffffff;
  border-radius: 6px;
  gap: 10px;
  box-shadow: 0px 1px 2px 0px #00000014;
  color: ${({ type }) =>
    type === TOAST_TYPE.SUCCESSFUL ? "#66e7c6" : "#FFA2A2"};
  padding: 1rem;
  justify-content: flex-start;
`;

export const CheckContainer = styled.span<{ type: TOAST_TYPE }>`
  width: 32px;
  height: 32px;
  padding: 6px;
  border-radius: 7px;
  background: ${({ type }) =>
    type === TOAST_TYPE.SUCCESSFUL ? "#ccf7ec" : "#FFE0E0"};
  box-sizing: border-box;
  color: #00ac80;
  ${flex}
`;
export const RightBox = styled.div`
  ${flex}
  justify-content: space-between;
  width: calc(100% - 32px + 10px);
`;
export const Message = styled.div<{ type: TOAST_TYPE }>`
  font-family: TT Hoves Pro;
  font-size: 14px;
  font-weight: 400;
  line-height: 21px;
  color: ${({ type }) =>
    type === TOAST_TYPE.SUCCESSFUL ? "#00ac80" : "#FF6464"};
`;
export const CloseButton = styled.button<{ toastType: TOAST_TYPE }>`
  ${flex}
  width: 18px;
  height: 18px;
  border: none;
  background-color: transparent;
  color: ${({ toastType }) =>
    toastType === TOAST_TYPE.SUCCESSFUL ? "#00ac80" : "#FF6464"};
  cursor: pointer;
  svg {
    font-size: 18px;
  }
`;
